import React from 'react';
import PropTypes from 'prop-types';

import './Icon.scss';

/**
 * Icon
 * @description [Description]
 * @example
  <div id="Icon"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Icon, {
    	title : 'Example Icon'
    }), document.getElementById("Icon"));
  </script>
 */
class Icon extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'icon';
	}

	render() {
		const {children, icon, height, width, className} = this.props;

		if (!icon && !children) {
			return null;
		}

		const atts = {
			className: this.baseClass + (className ? ` ${className}` : ''),
			'aria-hidden': true
		};

		if (children) {
			return <span {...atts}>{children}</span>;
		}

		if (typeof icon === 'string') {
			return (
				<svg viewBox={`0 0 ${width} ${height}`} {...atts}>
					<path d={icon} />
				</svg>
			);
		}

		if (typeof icon === 'function') {
			const Tag = icon;

			return <Tag {...atts} />;
		}

		return 'TODO';
	}
}

Icon.defaultProps = {
	height: 100,
	width: 100,
	className: '',
	icon: null,
	children: null
};

Icon.propTypes = {
	height: PropTypes.number,
	width: PropTypes.number,
	className: PropTypes.string,
	icon: PropTypes.any,
	children: PropTypes.node
};

export default Icon;
